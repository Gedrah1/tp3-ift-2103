﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

    public GameObject SettingScreen;
    public GameObject PauseScreen;
    public GameObject InGameScreen;
    public GameObject loadingCanvas;
    public Slider slider;
    public GameObject sfxSlider;
    public GameObject SoundManager;
    public GameObject backgroundSlider;
    public GameObject foleySlider;
    public GameObject delaySlider;
    public AudioClip pauseSong;
    public AudioClip confirmSound;
    public AudioClip cancelSound;
    public AudioClip navigateSound;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void playButtonSound(AudioClip audio)
    {
        GetComponent<AudioSource>().clip = audio;
        GetComponent<AudioSource>().Play();
    }

    public void SettingsButton()
    {
        playButtonSound(navigateSound);
        PauseScreen.SetActive(false);
        InGameScreen.SetActive(false);
        SettingScreen.SetActive(true);
        UpdateSliderValue();
    }

    public void ResumeButton()
    {
        PauseScreen.SetActive(false);
        InGameScreen.SetActive(true);
        SoundManager.GetComponent<SoundsManagerIsland>().removeBackgroundMenu();
        Time.timeScale = 1;
        playButtonSound(confirmSound);
    }

    public void QuitButton()
    {
        Time.timeScale = 1;
        playButtonSound(confirmSound);
        LoadLevel(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void QuitGameButton()
    {
        Application.Quit();
    }


    /// <summary>
    /// Settings button part
    /// </summary>

    public void NavigateToMainMenu()
    {
        PauseScreen.SetActive(true);
        SettingScreen.SetActive(false);
    }

    public void CancelButton()
    {
        playButtonSound(cancelSound);
        NavigateToMainMenu();
    }

    public void SaveButton()
    {
        SaveSlidersDatas();
        playButtonSound(confirmSound);
        SoundManager.GetComponent<SoundsManagerIsland>().SetBackgroundMusicVolume(SoundsClass.backgroundValue);
        SoundManager.GetComponent<SoundsManagerIsland>().SetFoleyVolume(SoundsClass.foleyValue);
        SoundManager.GetComponent<SoundsManagerIsland>().SetSoundsEffectVolume(SoundsClass.sfxValue);
        GetComponent<AudioSource>().volume = SoundsClass.sfxValue;
        NavigateToMainMenu();
    }

    private void SaveSlidersDatas()
    {
        SoundsClass.sfxValue = sfxSlider.GetComponent<Slider>().value;
        SoundsClass.backgroundValue = backgroundSlider.GetComponent<Slider>().value;
        SoundsClass.foleyValue = foleySlider.GetComponent<Slider>().value;
        SoundsClass.delayValue = delaySlider.GetComponent<Slider>().value;
    }

    private void UpdateSliderValue()
    {
        sfxSlider.GetComponent<Slider>().value = SoundsClass.sfxValue;
        backgroundSlider.GetComponent<Slider>().value = SoundsClass.backgroundValue;
        foleySlider.GetComponent<Slider>().value = SoundsClass.foleyValue;
        delaySlider.GetComponent<Slider>().value = SoundsClass.delayValue;
    }

    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        yield return null;

        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        loadingCanvas.SetActive(true);

        while (!operation.isDone)
        {
            new WaitForSeconds(1);
            float progress = Mathf.Clamp01(operation.progress / .9f);

            slider.value = progress;

            yield return null;
        }
    }

    public void PauseButton()
    {
        Time.timeScale = 0;
        InGameScreen.SetActive(false);
        PauseScreen.SetActive(true);
        SoundManager.GetComponent<SoundsManagerIsland>().SetBackgroundMusic();
    }
}
