﻿using Gaia;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public GameObject attackEffect;
    public AudioClip walk;
    public AudioClip run;
    public GameObject PauseMenu;
    public GameObject countDown;

    private float speed = 4f;
    private float rotSpeed = 80f;
    private float rot = 0f;
    private float gravity = 8f;
    private Rigidbody rBody;
    private float forwardInput, turnInput;

    private Vector3 moveDir = Vector3.zero;

    private CharacterController controller;
    private Animator anim;
    private AudioSource footstep;

    
    // Use this for initialization
    void Start()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        footstep = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (countDown.GetComponent<DelayStart>().startGame)
        {
            if (controller.isGrounded)
            {
                if (Input.GetKey(KeyCode.P))
                {
                    PauseMenu.GetComponent<PauseMenu>().PauseButton();
                }
                if (Input.GetKey(KeyCode.UpArrow) && anim.GetInteger("condition") != 6)
                {
                    anim.SetInteger("condition", 2);
                    moveDir = new Vector3(0, 0, 1);
                    moveDir = moveDir * speed;
                    moveDir = transform.TransformDirection(moveDir);
                    manageFootstep();
                }
                else if (Input.GetKey(KeyCode.DownArrow) && anim.GetInteger("condition") != 6)
                {
                    anim.SetInteger("condition", 3);
                    moveDir = new Vector3(0, 0, -1);
                    moveDir = moveDir * speed;
                    moveDir = transform.TransformDirection(moveDir);
                    manageFootstep();
                }
                else if (Input.GetKey(KeyCode.LeftArrow) && anim.GetInteger("condition") == 0)
                {
                    anim.SetInteger("condition", 5);
                }
                else if (Input.GetKey(KeyCode.RightArrow) && anim.GetInteger("condition") == 0)
                {
                    anim.SetInteger("condition", 4);
                }
                else if (Input.GetKey(KeyCode.Space) && anim.GetInteger("condition") == 0)
                {
                    anim.SetInteger("condition", 6);
                    var hand = GameObject.Find("Hand");
                    Instantiate(attackEffect, hand.transform.position, new Quaternion(0f, 0f, 0f, 0f));
                }
                if (Input.GetKeyUp(KeyCode.UpArrow))
                {
                    anim.SetInteger("condition", 0);
                    moveDir = new Vector3(0, 0, 0);
                    manageFootstep();
                }
                else if (Input.GetKeyUp(KeyCode.DownArrow))
                {
                    anim.SetInteger("condition", 0);
                    moveDir = new Vector3(0, 0, 0);
                    manageFootstep();
                }
                else if (Input.GetKeyUp(KeyCode.RightArrow) && !Input.GetKey(KeyCode.UpArrow))
                {
                    anim.SetInteger("condition", 0);
                    moveDir = new Vector3(0, 0, 0);
                }
                else if (Input.GetKeyUp(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.UpArrow))
                {
                    anim.SetInteger("condition", 0);
                    moveDir = new Vector3(0, 0, 0);
                }
                else if (Input.GetKeyUp(KeyCode.Space))
                {
                    anim.SetInteger("condition", 0);
                    moveDir = new Vector3(0, 0, 0);
                }

            }

            rot += Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;
            transform.eulerAngles = new Vector3(0, rot, 0);
            moveDir.y -= gravity * Time.deltaTime;
            controller.Move(moveDir * Time.deltaTime);
        }
    }

    private void manageFootstep()
    {
      if (anim.GetInteger("condition") == 2) {
            if (!footstep.isPlaying)
            {
                footstep.clip = run;
                footstep.Play();
            }
        } else if (anim.GetInteger("condition") == 3) {
            if (!footstep.isPlaying)
            {
                footstep.clip = walk;
                footstep.Play();
            }
        } else {
            footstep.Stop();
        }
    }
}
