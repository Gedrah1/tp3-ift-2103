﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsManagerIsland : MonoBehaviour {

    private AudioSource backgroundMusic;
    private AudioSource foleys;
    private List<AudioSource> soundEffects = new List<AudioSource>();

    public AudioClip alertMusic;
    public AudioClip FanfareVictoryMusic;
    public AudioClip defeatMusic;
    public AudioClip pauseSong;
    public AudioClip ForestSong;
    public AudioClip introMusic;
    public AudioClip alertSound;

    private AudioSource confirmSound;
    private AudioSource cancelSound;
    private AudioSource navigateSound;

    private float currentClipTime;
    private int currentBgMusic = -1;

    private void Awake()
    {
        var gameObjectSFX = GameObject.FindGameObjectsWithTag("SFX");
        var gameObjectFoley = GameObject.FindGameObjectWithTag("Foley");

        foleys = gameObjectFoley.GetComponent<AudioSource>();
        backgroundMusic = GetComponent<AudioSource>();
        foreach (var objects in gameObjectSFX)
        {
            soundEffects.Add(objects.GetComponent<AudioSource>());
        }
    }

    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update() {

    }

    public void SetAlarm()
    {
        backgroundMusic.clip = alertMusic;
        backgroundMusic.Play();
    }

    public void playAlertSound()
    {
        backgroundMusic.clip = alertSound;
        backgroundMusic.Play();
    }

    public void SetBackgroundMusicVolume(float volume)
    {
        backgroundMusic.volume = volume;
    }

    public void SetSoundsEffectVolume(float volume)
    {
        foreach (var sounds in soundEffects)
        {
            sounds.volume = volume;
        }
    }

    public void SetFoleyVolume(float volume)
    {
        foleys.volume = volume;
    }

    public void PlayConfirmSoundButton()
    {
        confirmSound.Play();
    }

    public void PlayCancelSoundButton()
    {
        cancelSound.Play();
    }

    public void PlayNavigateSoundButton()
    {
        navigateSound.Play();
    }

    public void SetBackgroundMusic()
    {
        currentClipTime = backgroundMusic.time;

        if (backgroundMusic.clip.name == alertMusic.name)
        {
            currentBgMusic = 0;
        }
        else if (backgroundMusic.clip.name == FanfareVictoryMusic.name)
        {
            currentBgMusic = 1;
        }
        else if (backgroundMusic.clip.name == defeatMusic.name)
        {
            currentBgMusic = 2;
        }
        else if (backgroundMusic.clip.name == ForestSong.name)
        {
            currentBgMusic = 3;
        }
        backgroundMusic.clip = pauseSong;
        backgroundMusic.Play();
    }


    public void playDefeatSong()
    {
        backgroundMusic.clip = defeatMusic;
        backgroundMusic.Play();
    }

    public void playIntroSong()
    {
        backgroundMusic.clip = introMusic;
        backgroundMusic.pitch = 1.1f;
        backgroundMusic.loop = false; 
        backgroundMusic.Play();
    }

    public void playBgSong()
    {
        backgroundMusic.clip = ForestSong;
        backgroundMusic.pitch = 1f;
        backgroundMusic.loop = true;
        backgroundMusic.Play();
    }

    public void playVictorySong()
    {
        backgroundMusic.clip = FanfareVictoryMusic;
        backgroundMusic.Play();
    }


    public void removeBackgroundMenu()
    {
        if (currentBgMusic == 0)
        {
            backgroundMusic.clip = alertMusic;
        }
        else if (currentBgMusic == 1)
        {
            backgroundMusic.clip = FanfareVictoryMusic;
        }
        else if (currentBgMusic == 2)
        {
            backgroundMusic.clip = defeatMusic;
        }
        else if (currentBgMusic == 3)
        {
            backgroundMusic.clip = ForestSong;
        }
        backgroundMusic.Play();
        backgroundMusic.time = currentClipTime;
    }
}
