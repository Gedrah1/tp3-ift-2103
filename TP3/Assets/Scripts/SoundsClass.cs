﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsClass {
    public static float sfxValue = 0.400f;
    public static float backgroundValue = 0.200f;
    public static float foleyValue = 0.400f;
    public static float delayValue = 120f;
}
