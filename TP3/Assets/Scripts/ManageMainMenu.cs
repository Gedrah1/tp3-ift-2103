﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ManageMainMenu : MonoBehaviour {

    public GameObject MainScreen;
    public GameObject SettingScreen;

    public GameObject sfxSlider;
    public GameObject SoundManager;
    public GameObject backgroundSlider;
    public GameObject foleySlider;
    public GameObject delaySlider;

    public GameObject loadingCanvas;
    public Slider slider;

    public GameObject settingsButton;
    public GameObject settingsParticule;

    public GameObject playButton;
    public GameObject playParticule;

    public GameObject quitButton;
    public GameObject quitParticule;

    public GameObject cancelButton;
    public GameObject saveButton;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// Main Menu part
    /// </summary>
    /// 

    private void ResetButton(GameObject button, GameObject particule)
    {
        Color co = new Color(0.08721964f, 0.1635607f, 0.5283019f);
        button.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
        button.GetComponent<Image>().color = co;
        if (particule)
        {
            particule.SetActive(false);
        }
    }

    public void SettingsButton()
    {
        SoundManager.GetComponent<SoundManager>().PlayNavigateSoundButton();
        ResetButton(settingsButton, settingsParticule);
        MainScreen.SetActive(false);
        SettingScreen.SetActive(true);
        UpdateSliderValue();
    }

    public void PlayButton()
    {
        SoundManager.GetComponent<SoundManager>().PlayConfirmSoundButton();
        ResetButton(playButton, playParticule);
        LoadLevel(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitButton()
    {
        SoundManager.GetComponent<SoundManager>().PlayConfirmSoundButton();
        ResetButton(quitButton, quitParticule);
        Application.Quit();
    }


    /// <summary>
    /// Settings button part
    /// </summary>

    public void NavigateToMainMenu()
    {
        MainScreen.SetActive(true);
        SettingScreen.SetActive(false);
    }

    public void CancelButton()
    {
        SoundManager.GetComponent<SoundManager>().PlayCancelSoundButton();
        ResetButton(cancelButton, null);
        NavigateToMainMenu();
    }

    public void SaveButton()
    {
        SaveSlidersDatas();
        SoundManager.GetComponent<SoundManager>().PlayConfirmSoundButton();
        SoundManager.GetComponent<SoundManager>().SetBackgroundMusicVolume(SoundsClass.backgroundValue);
        SoundManager.GetComponent<SoundManager>().SetSoundsEffectVolume(SoundsClass.sfxValue);
        ResetButton(saveButton, null);
        NavigateToMainMenu();
    }

    private void SaveSlidersDatas()
    {
        SoundsClass.sfxValue = sfxSlider.GetComponent<Slider>().value;
        SoundsClass.backgroundValue = backgroundSlider.GetComponent<Slider>().value;
        SoundsClass.foleyValue = foleySlider.GetComponent<Slider>().value;
        SoundsClass.delayValue = delaySlider.GetComponent<Slider>().value;
    }

    private void UpdateSliderValue()
    {
        sfxSlider.GetComponent<Slider>().value = SoundsClass.sfxValue;
        backgroundSlider.GetComponent<Slider>().value = SoundsClass.backgroundValue;
        foleySlider.GetComponent<Slider>().value = SoundsClass.foleyValue;
        delaySlider.GetComponent<Slider>().value = SoundsClass.delayValue;
    }

    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        yield return null;

        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        loadingCanvas.SetActive(true);

        while (!operation.isDone)
        {
            new WaitForSeconds(1);
            float progress = Mathf.Clamp01(operation.progress / .9f);

            slider.value = progress;

            yield return null;
        }
    }
}
