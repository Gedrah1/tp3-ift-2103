﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ContentManager : MonoBehaviour {

    public GameObject slideBar;
    public GameObject sliderText;
    public GameObject player;
    public GameObject fireCircle;
    public GameObject SoundManager;
    public GameObject countDown;
    public GameObject defeatScreen;
    public GameObject victoryScreen;
    public GameObject alertImage;

    private float time;
    private AudioSource audioSource;
    private int min = 1;
    private float realMinutes = 0f;
    private bool alertSet = false;
    private Animator alertAnimator;

    // 3 randoms positions on the map
    private Vector3 positionFlame1 = new Vector3(133.4326f, 35.0299f, -141.2214f);
    private Vector3 positionFlame2 = new Vector3(-28.50761f, 17.02883f, 177.5452f);
    private Vector3 positionFlame3 = new Vector3(-68.90689f, 34.53124f, -46.71237f);

    private void Awake()
    {
         var pos = UnityEngine.Random.Range(1, 3);
         if (pos == 1) {
             fireCircle = Instantiate(fireCircle, positionFlame1, new Quaternion(0f, 0f, 0f, 0f));
         } else if (pos == 2) {
             fireCircle = Instantiate(fireCircle, positionFlame2, new Quaternion(0f, 0f, 0f, 0f));
         }
         else if (pos == 3) {
             fireCircle = Instantiate(fireCircle, positionFlame3, new Quaternion(0f, 0f, 0f, 0f));
         }
    }

    // Use this for initialization
    void Start () {
        SoundManager.GetComponent<SoundsManagerIsland>().SetBackgroundMusicVolume(SoundsClass.backgroundValue);
        SoundManager.GetComponent<SoundsManagerIsland>().SetFoleyVolume(SoundsClass.foleyValue);
        SoundManager.GetComponent<SoundsManagerIsland>().SetSoundsEffectVolume(SoundsClass.sfxValue);

        SoundManager.GetComponent<SoundsManagerIsland>().playIntroSong();
        countDown.GetComponent<DelayStart>().StartRace(SoundManager.GetComponent<SoundsManagerIsland>());
        slideBar.GetComponent<Slider>().maxValue = SoundsClass.delayValue;
        alertAnimator = alertImage.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update () {
        if (countDown.GetComponent<DelayStart>().startGame)
        {
            manageTime();
        }
    }


    void manageTime()
    {
        time += Time.deltaTime;
        slideBar.GetComponent<Slider>().value = time;
        if (slideBar.GetComponent<Slider>().value > (SoundsClass.delayValue - (SoundsClass.delayValue / 3)) && !alertSet)
        {
            alertSet = true;
            alertImage.SetActive(true);
            SoundManager.GetComponent<SoundsManagerIsland>().playAlertSound();
            countDown.GetComponent<DelayStart>().StartAlertMode();
        }
        if (alertSet && countDown.GetComponent<DelayStart>().alertSoundRun)
        {
            alertAnimator.SetBool("Normal", !alertAnimator.GetBool("Normal")); 
        }
        var seconds = time % 60;
        var minutes = time / 60;
        var fraction = (time * 100) % 100;

        if (time < (60 * min))
        {
            minutes = realMinutes;
        }
        else
        {
            min++;
            realMinutes++;
        }
        Debug.Log(slideBar.GetComponent<Slider>().value);
        if (fireCircle.GetComponent<FlameOnTrigger>().playerWin)
        {
            showVictoryScreen();
        }
        else if (slideBar.GetComponent<Slider>().value >= SoundsClass.delayValue)
        {
            sliderText.GetComponent<Text>().text = "00 : 00 : 00";
            showDefeatScreen();
        }
        else
        {
            sliderText.GetComponent<Text>().text = string.Format("{0:00} : {1:00} : {2:00}", (SoundsClass.delayValue / 60) - 1 - minutes, 60 - seconds, 100 - fraction);
        }
    }

    private void showDefeatScreen()
    {
        if (!defeatScreen.activeInHierarchy)
        {
            defeatScreen.SetActive(true);
            SoundManager.GetComponent<SoundsManagerIsland>().playDefeatSong();
            Time.timeScale = 0;
        }
    }


    private void showVictoryScreen()
    {
        if (!victoryScreen.activeInHierarchy)
        {
            victoryScreen.SetActive(true);
            SoundManager.GetComponent<SoundsManagerIsland>().playVictorySong();
            Time.timeScale = 0;
        }
    }
}
