﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    private AudioSource backgroundMusic;
    private AudioSource confirmSound;
    private AudioSource cancelSound;
    private AudioSource navigateSound;

    // Use this for initialization
    void Start () {
        var audioList = GetComponents<AudioSource>();
        foreach (var audio in audioList)
        {
            switch (audio.clip.name)
            {
                case "menu":
                    backgroundMusic = audio;
                    break;
                case "confirmation":
                    confirmSound = audio;
                    break;
                case "navigate":
                    navigateSound = audio;
                    break;
                case "cancel":
                    cancelSound = audio;
                    break;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetBackgroundMusicVolume(float volume)
    {
        backgroundMusic.volume = volume;
    }

    public void SetSoundsEffectVolume(float volume)
    {
        cancelSound.volume = volume;
        navigateSound.volume = volume;
        confirmSound.volume = volume;
    }

    public void PlayConfirmSoundButton()
    {
        confirmSound.Play();
    }

    public void PlayCancelSoundButton()
    {
        cancelSound.Play();
    }

    public void PlayNavigateSoundButton()
    {
        navigateSound.Play();
    }
}
