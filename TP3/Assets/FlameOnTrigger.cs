﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameOnTrigger : MonoBehaviour {

    public bool playerWin = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter(Collider trigger)
    {
        playerWin = true;
    }
}
