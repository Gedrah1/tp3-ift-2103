﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayStart : MonoBehaviour {

    public GameObject countDown;
    public GameObject AlertImage;
    public bool startGame = false;
    public bool alertSoundRun = true;
    private SoundsManagerIsland manager;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartRace(SoundsManagerIsland sdManager)
    {
        manager = sdManager;
        StartCoroutine("startDelay");
    }

    public void StartAlertMode()
    {
        StartCoroutine("startAlert");
    }

    IEnumerator startDelay()
    {
        float pauseTime = Time.realtimeSinceStartup + 4f;
        while (Time.realtimeSinceStartup < pauseTime)
        {
            yield return 0;
        }
        countDown.gameObject.SetActive(false);
        startGame = true;
        manager.playBgSong();
    }

    IEnumerator startAlert()
    {
        float pauseTime = Time.realtimeSinceStartup + 5f;
        while (Time.realtimeSinceStartup < pauseTime)
        {
            yield return 0;
        }
        AlertImage.SetActive(false);
        alertSoundRun = false;
        manager.SetAlarm();
    }
}
